if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    # extract CUDA version from package name so
    # we know what the executable is called
    CUDA_VER=${PKG_NAME/fsl-fdt-cuda-/}
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper xfibres_gpu${CUDA_VER}
fi

if [ -e ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ]; then
  CUDA_VER=${PKG_NAME/fsl-fdt-cuda-/}
  ${FSLDIR}/share/fsl/sbin/removeFSLWrapper xfibres_gpu${CUDA_VER}
fi
